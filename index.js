var mousePressed = false;
var lastX,lastY;
var staticX,staticY;
var btn_sel = "Brush";
var cPushArray = new Array();
var cStep = -1;
var snapshot;
var dragging;
var hasInput;

function change_brush_size(brush_size){
    document.getElementById("size_sel_p").innerText = brush_size + " px";
}

function button_sel(btn){
    if(btn == "Brush" || btn == "Eraser" || btn == "Text"
    || btn == "Circle" || btn == "Triangle" || btn == "Rectangle"){
        btn_sel = btn;
    }else{
        btn_sel = "none";
    }
}

function takeSnapshot(){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    snapshot = ctx.getImageData(0,0,canvas.width,canvas.height);
}
function restoreSnapshot() {
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    ctx.putImageData(snapshot, 0, 0);
}
function md(e){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    mousePressed = true;
    var x = e.pageX - canvas.offsetLeft;
    var y = e.pageY - canvas.offsetTop;
    dragging = true;
    if(btn_sel == "Brush" || btn_sel == "Eraser")
        Draw(x,y,false);
    else if(btn_sel == "Text"){
        if(hasInput) return;
        Text_input(x,y);
    }
    else if(btn_sel == "Rectangle"){
        takeSnapshot();
        Draw_rec(x,y,false);
        staticX = x;
        staticY = y;
    }else if(btn_sel == "Triangle"){
        takeSnapshot();
        Draw_rec(x,y,false);
        staticX = x;
        staticY = y;
    }else if(btn_sel == "Circle"){
        takeSnapshot();
        Draw_cir(x,y,false);
        staticX = x;
        staticY = y;
    }
    //console.log(x,y);
    console.log(btn_sel);

}

function mm(e){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    //cursor icon
    if(btn_sel == "Brush")
        canvas.style.cursor = "url(cursor/brush.png) , auto";
    else if(btn_sel == "Eraser")
        canvas.style.cursor = "url(cursor/eraser.png) , auto";
    else if(btn_sel == "Text")
        canvas.style.cursor = "text";
    else if(btn_sel == "Triangle")
        canvas.style.cursor = "url(cursor/triangle.png) , auto";
    else if(btn_sel == "Circle")
        canvas.style.cursor = "url(cursor/circle.png) , auto";
    else if(btn_sel == "Rectangle")
        canvas.style.cursor = "url(cursor/square.png) , auto";

    if(mousePressed){
        var x = e.pageX - canvas.offsetLeft;
        var y = e.pageY - canvas.offsetTop;
        if(btn_sel == "Brush" || btn_sel == "Eraser")
            Draw(x,y,true);
        else if(btn_sel == "Rectangle"){
            Draw_rec(x,y,true);
        }else if(btn_sel == "Triangle"){
            Draw_tri(x,y,true);
        }else if(btn_sel == "Circle"){
            Draw_cir(x,y,true);
        }
        //console.log(x,y);
        console.log(btn_sel);
    }
}

function mu(e){
    var canvas = document.getElementById('Canvas');
    var x = e.pageX - canvas.offsetLeft;
    var y = e.pageY - canvas.offsetTop;
    dragging = false;
    mousePressed = false;
    if(btn_sel == "Rectangle")
        Draw_rec(x,y,true);
    else if(btn_sel == "Triangle")
        Draw_tri(x,y,true);
    else if(btn_sel == "Circle")
        Draw_cir(x,y,true);
    if(btn_sel != "Text")
        cPush();
}

function ml(e){
    if(mousePressed && btn_sel != "Text")
        cPush();
    mousePressed = false;
}

function Draw(x ,y ,isDown){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    if(btn_sel == "Eraser") ctx.globalCompositeOperation = "destination-out";
    else ctx.globalCompositeOperation = "source-over";

    if(isDown){
        ctx.beginPath();
        if(btn_sel == "Eraser") ctx.strokeStyle = "#ffffff";
        else ctx.strokeStyle = document.getElementById("colorpicker").value;
        ctx.lineWidth = document.getElementById("size_sel").value;
        ctx.lineJoin = "round";
        ctx.moveTo(lastX,lastY);
        ctx.lineTo(x,y);
        ctx.closePath();
        ctx.stroke();
    }
    lastX = x; lastY = y;
    ctx.globalCompositeOperation = "source-over"
}

function _Text(x,y,txt){
    console.log(txt);
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    var font = document.getElementById('font').value;
    var font_size = document.getElementById('font_size').value;
    ctx.globalCompositeOperation = "source-over";
    ctx.font = font_size + "px " + font;
    console.log(font_size + "px " + font);
    ctx.fillStyle = document.getElementById("colorpicker").value;
    ctx.textAlign = "left";
    ctx.fillText(txt,x,y);
    cPush();
}

function Text_input(x,y){
    var input = document.createElement('input');
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = x + 'px';
    input.style.top = (y+10) +'px';
    document.body.appendChild(input);
    hasInput = true;
    input.focus();

    input.onkeydown = function(e){
        var keycode = e.key;
        if(keycode == "Enter"){
            _Text(x,y,this.value);
            document.body.removeChild(this);
            hasInput = false;
        }
    }
}

function Draw_rec(x,y,isDown){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    if(dragging) restoreSnapshot();
    if(isDown){
        ctx.beginPath();
        ctx.strokeStyle = document.getElementById("colorpicker").value;
        ctx.lineWidth = document.getElementById('size_sel').value;
        ctx.rect(staticX,staticY,x - staticX, y - staticY);
        ctx.stroke();
    }
    lastX = x; lastY = y;
}

function Draw_tri(x,y,isDown){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    if(dragging) restoreSnapshot();
    if(isDown){
        ctx.beginPath();
        ctx.strokeStyle = document.getElementById("colorpicker").value;
        ctx.lineWidth = document.getElementById('size_sel').value;
        ctx.moveTo(staticX,staticY);
        ctx.lineTo(x,staticY);
        ctx.lineTo((x+staticX)/2,y);
        ctx.closePath();
        ctx.stroke();
    }
    lastX = x; lastY = y;
}

function Draw_cir(x,y,isDown){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    if(dragging) restoreSnapshot();
    if(isDown){
        ctx.beginPath();
        ctx.strokeStyle = document.getElementById("colorpicker").value;
        ctx.lineWidth = document.getElementById('size_sel').value;
        ctx.arc((staticX+x)/2 , (staticY+y)/2,(Math.abs(staticX-x)/2),0,Math.PI*2,true);
        ctx.stroke();
    }
    lastX = x; lastY = y;
}
function Refresh(){
    var canvas = document.getElementById('Canvas');
    canvas.width = "800";
    canvas.height = "600";
    cStep = 0;
    Clear();
}
function Clear(){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0,0,canvas.width,canvas.height);
}

function cPush(){
    cStep++;
    console.log(cStep);
    if(cStep < cPushArray.length)
        cPushArray.length = cStep;
    cPushArray.push(document.getElementById('Canvas').toDataURL());
}

function cUndo(){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    if(cStep > 0){
        cStep--;
        console.log(cStep);
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function (){
            Clear();
            ctx.drawImage(canvasPic,0,0);
        }
    }else if(cStep == 0){
        Clear();
        cStep--;
    }
}

function cRedo(){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    if(cStep < cPushArray.length-1){
        cStep++;
        console.log(cStep);
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function (){
            Clear();
            ctx.drawImage(canvasPic,0,0);
        }
    }else if(cStep == -1){
        cStep++;
        console.log(cStep);
        var canvasPic = new Image();
        if(cPushArray[cStep] != null){
            canvasPic.src = cPushArray[cStep];
            canvasPic.onload = function (){
                Clear();
                ctx.drawImage(canvasPic,0,0);
            }
        }
    }
}

function upload_photo(e){
    var canvas = document.getElementById('Canvas');
    var ctx = canvas.getContext('2d');
    var img = new Image();
    img.src = URL.createObjectURL(document.getElementById('upload').files[0]);
    img.onload = function(){
        canvas.width = img.width;
        canvas.height = img.height;
        Clear();
        ctx.drawImage(img,0,0);
    }
    img.onerror = function(){console.error("Can't read the photo");}
}

function download_img(e){
    var canvas = document.getElementById('Canvas');
    var image = canvas.toDataURL("image/jpg");
    e.href = image;
}
