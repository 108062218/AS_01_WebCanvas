# Hw01_Canvas

# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
    use pencil to draw.
    use eraser to erase.
    use text input button to draw text.
    use triangle tool to draw triangle.
    use rectangle tool to draw rectangle.
    use circle tool to draw circle.
    use brush size scroll bar to chage the brush size.
    use colorpicker to pick color.
    use font selector to select font.
    use font size selector to select font size.
    use Undo button to undo.
    use Redo button to redo.
    use Refresh button to refresh the canvas.
    use upload button to upload image to canvas.
    use download button to download image from canvas.
### Function description

    I didn't make any bonus function :( .

### Gitlab page link

    https://108062218.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Thank you TA OuO.
